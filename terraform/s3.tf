# TODO : Create a s3 bucket with aws_s3_bucket
/*
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
*/

resource "aws_s3_bucket" "s3-job-offer-bucket-comoma" {
  bucket = var.s3_user_bucket_name


  acl = "public-read" //pas private
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "job_offer" {
  bucket = var.s3_user_bucket_name
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-comoma.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix = aws_s3_bucket_object.job_offer.key
    filter_suffix = ".csv"
  }
  depends_on = [ aws_lambda_permission.allow_bucket ]
}

/*
resource "aws_cloudwatch_event_rule" "every_one_minute" {
  name                = "every-one-minute"
  description         = "Fires every one minutes"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "check_foo_every_one_minute" {
  rule      = aws_cloudwatch_event_rule.every_one_minute.name
  target_id = "lambda"
  arn       = aws_lambda_function.test_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.every_one_minute.arn
}
*/